create schema core;

create table core.traffic (
  unit_packet_id bigserial primary key,
  lon            numeric not null,
  lat            numeric not null
);

create table core.incident (
  incident_id    bigserial primary key,
  reason         text not null,
  unit_packet_id bigint
);