package ru.advantum.csptm.database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbMigratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbMigratorApplication.class, args);
    }

}
