package ru.advantum.csptm.service.coordssaver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import ru.advantum.csptm.database.api.CoordsHub;

@SpringBootApplication
@EnableBinding(CoordsSaver.class)
public class CoordsSaverApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoordsSaverApplication.class, args);
    }


}
