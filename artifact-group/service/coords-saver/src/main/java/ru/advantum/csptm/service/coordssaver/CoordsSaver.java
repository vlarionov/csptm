package ru.advantum.csptm.service.coordssaver;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import ru.advantum.csptm.database.api.CoordsHub;

public interface CoordsSaver {
    String INPUT = CoordsHub.OUTPUT;

    @Input(INPUT)
    SubscribableChannel coords();

}
