package ru.advantum.csptm.service.coordssaver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.database.api.CoordsHub;
import ru.advantum.csptm.database.api.UnitPacket;

@Service
public class CoordsSaverService {
    private static final String SQL = "insert into core.traffic(lon, lat) values (?,?)";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CoordsSaverService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @StreamListener(CoordsHub.OUTPUT)
    public void process(UnitPacket unitPacket) {
        jdbcTemplate.update(SQL, unitPacket.getLon(), unitPacket.getLat());
    }

}
