package ru.advantum.csptm.service.incidentsssaver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.database.api.CoordsHub;
import ru.advantum.csptm.database.api.UnitPacket;
import ru.advantum.csptm.service.incident.api.Incident;

@Service
public class IncidentsSaverService {
    private static final String SQL = "insert into core.incident(reason) values (?)";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public IncidentsSaverService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @StreamListener(IncidentsSaver.INPUT)
    public void process(Incident incident) {
        jdbcTemplate.update(SQL, incident.getReason());
    }

}
