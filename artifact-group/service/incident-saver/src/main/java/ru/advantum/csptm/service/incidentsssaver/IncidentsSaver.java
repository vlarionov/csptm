package ru.advantum.csptm.service.incidentsssaver;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import ru.advantum.csptm.database.api.CoordsHub;
import ru.advantum.csptm.service.incident.api.IncidentProducer;

public interface IncidentsSaver {
    String INPUT = IncidentProducer.OUTPUT;

    @Input(INPUT)
    SubscribableChannel incidents();

}
