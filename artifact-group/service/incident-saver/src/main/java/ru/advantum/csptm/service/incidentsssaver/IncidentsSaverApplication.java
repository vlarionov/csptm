package ru.advantum.csptm.service.incidentsssaver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import ru.advantum.csptm.database.api.CoordsHub;
import ru.advantum.csptm.service.incidentsssaver.IncidentsSaver;

@SpringBootApplication
@EnableBinding(IncidentsSaver.class)
public class IncidentsSaverApplication {

    public static void main(String[] args) {
        SpringApplication.run(IncidentsSaverApplication.class, args);
    }


}
