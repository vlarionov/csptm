package ru.advantum.csptm.service.incident;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.database.api.UnitPacket;
import ru.advantum.csptm.service.incident.api.Incident;
import ru.advantum.csptm.service.incident.api.IncidentProducer;

@Component
public class IncidentProducerService {
    private final MessageChannel channel;

    @Autowired
    public IncidentProducerService(@Qualifier(IncidentProducer.OUTPUT) MessageChannel channel) {
        this.channel = channel;
    }

    @StreamListener(IncidentProducer.INPUT)
    public void process(UnitPacket unitPacket) {
        if (unitPacket.getLon() < 0) {
            channel.send(MessageBuilder.withPayload(new Incident("lon is negative", unitPacket)).build());
        }
    }

}
