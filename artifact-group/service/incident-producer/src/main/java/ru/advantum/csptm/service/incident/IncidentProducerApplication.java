package ru.advantum.csptm.service.incident;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import ru.advantum.csptm.service.incident.api.IncidentProducer;

@SpringBootApplication
@EnableBinding(IncidentProducer.class)
public class IncidentProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IncidentProducerApplication.class, args);
    }

}
