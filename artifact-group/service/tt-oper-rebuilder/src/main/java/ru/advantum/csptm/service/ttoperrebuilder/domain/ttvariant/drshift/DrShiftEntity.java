package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtOut;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Set;
import java.util.SortedSet;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DrShiftEntity extends AbstractEntity<Short> implements DrShift {

    short drShiftNum;

    @Transient
    TtOut ttOut;

    @Override
    public Set<TtAction> getTtActions() {
        return getTtOut().getTtActions();
    }

}
