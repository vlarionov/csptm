package ru.advantum.csptm.service.ttoperrebuilder.configuration;

import lombok.val;
import org.hibernate.boot.model.naming.EntityNaming;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitBasicColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinColumnNameSource;
import org.hibernate.cfg.Ejb3Column;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;

import java.lang.reflect.Field;

public class PersistentImplicitNamingStrategy extends SpringImplicitNamingStrategy {

    @Override
    protected String transformEntityName(EntityNaming entityNaming) {
        return transformEntityName(super.transformEntityName(entityNaming));
    }

    @Override
    public Identifier determineBasicColumnName(ImplicitBasicColumnNameSource source) {
        Identifier result = super.determineBasicColumnName(source);
        if ("id".equals(source.getAttributePath().getProperty())) {
            result = toIdentifier(transformEntityName(getOwningEntityName(source)) + "Id", source.getBuildingContext());
        }
        return result;
    }

    @Override
    public Identifier determineJoinColumnName(ImplicitJoinColumnNameSource source) {
        final String name;
        if (source.getNature() == ImplicitJoinColumnNameSource.Nature.ELEMENT_COLLECTION || source.getAttributePath() == null) {
            name = transformEntityName(source.getEntityNaming()) + "_id";
        } else {
            name = transformAttributePath(source.getAttributePath()) + "_id";
        }

        return toIdentifier(name, source.getBuildingContext());
    }

    private String transformEntityName(String entityName) {
        return entityName.replace("Entity", "");
    }

    private String getOwningEntityName(ImplicitBasicColumnNameSource source) {
        val owningEntityFullyQualifiedName = getEjb3Column(source).getPropertyHolder().getPath();
        final String[] owningEntityTokens = owningEntityFullyQualifiedName.split("\\.");
        return owningEntityTokens[owningEntityTokens.length - 1];
    }

    private Ejb3Column getEjb3Column(ImplicitBasicColumnNameSource source) {
        Field ejb3ColumnField = null;
        final Field[] sourceFields = source.getClass().getDeclaredFields();
        for (final Field sourceField : sourceFields) {
            if (sourceField.getName().equals("this$0")) {
                ejb3ColumnField = sourceField;
            }
        }
        ejb3ColumnField.setAccessible(true);

        // Get actual field object
        String owningEntityName;
        Ejb3Column ejb3Column;
        try {
            return (Ejb3Column) ejb3ColumnField.get(source);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);  // (Or deal with this appropriately, e.g. log it.)
        }
    }
}
