package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import lombok.val;
import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;

import java.time.Duration;
import java.time.Instant;

public interface TimePeriod extends Comparable<TimePeriod> {
     default Instant getStartTime() {
        return getEndTime().minus(getDuration());
    }

    default Duration getDuration() {
        return Duration.between(getStartTime(), getEndTime());
    }

    default Instant getEndTime() {
        return getStartTime().plus(getDuration());
    }


    @Override
    default int compareTo(TimePeriod other) {
        val startTimeDiff = this.getStartTime().compareTo(other.getStartTime());
        if (startTimeDiff != 0) {
            return startTimeDiff;
        }
        val durationDiff = this.getDuration().compareTo(other.getDuration());
        if (durationDiff != 0) {
            return durationDiff;
        }

        Entity<Number> self = (Entity<Number>) this;
        Entity<Number> otherEntity = (Entity<Number>) other;
        return self.getId().intValue() - otherEntity.getId().intValue();
    }

}
