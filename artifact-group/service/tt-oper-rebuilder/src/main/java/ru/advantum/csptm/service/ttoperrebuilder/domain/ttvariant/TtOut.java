package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype.ActionType;

import java.util.Set;

public interface TtOut extends Entity<Integer>, TimePeriod, TtVariantEventListener {
    Integer getTtVariantId();

    TtVariant getTtVariant();

    short getTtOutNum();

    Set<TtAction> getTtActions();

    TtAction addTtAction(ActionType actionType);

//
//    NavigableSet<TtAction> getSortedTtActions();
//
//    Instant getStartTime();
//
//    Collection<TtActionItem> getTtActionItems();
//
//    NavigableSet<TtActionItem> getSortedTtActionItems();


}
