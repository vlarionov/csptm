package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import ru.advantum.csptm.service.ttoperrebuilder.domain.*;
import ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype.ActionTypeEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift.DrShift;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem2round.StopItem2Round;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TtActionEntity extends AbstractEntity<Integer> implements TtAction {
    Integer ttOutId;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ActionTypeEntity.class)
    @Fetch(FetchMode.JOIN)
    ActionType actionType;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ttActionId", targetEntity = TtActionEntity.class)
    @Fetch(FetchMode.JOIN)
    @Where(clause = "not sign_deleted")
    Set<TtActionItem> ttActionItems;

    @Transient
    TtOut ttOut;

    @Transient
    TtVariant ttVariant;

    public TtActionEntity(TtAction ttAction) {
        this(
                ttAction.getId(),
                ttAction.getTtOutId(),
                ttAction.getActionType(),
                ttAction.getTtActionItems().stream().map(TtActionItemEntity::new).collect(Collectors.toSet())
        );

    }

    public TtActionEntity(Integer id, Integer ttOutId, ActionType actionType, Set<TtActionItem> ttActionItems) {
        super(id);
        this.ttOutId = ttOutId;
        this.actionType = actionType;
        this.ttActionItems = ttActionItems;
    }

    public void setTtOut(TtOut ttOut) {
        this.ttOut = ttOut;
        setTtVariant(ttOut.getTtVariant());
    }

    @Override
    public TtActionItem addTtActionItem(Instant timeBegin, Instant timeEnd, StopItem2Round stopItem2Round, DrShift drShift) {
//        val ttActionItem = new TtActionItemEntity(
//                null,
//                getId(),
//                timeBegin,
//                timeEnd,
//                stopItem2Round,
//                drShift
//        );

//        return ttActionItem;
        return null;
    }

    @Override
    public void onEvent(TtActionItemDurationChangedEvent event) {

    }
}
