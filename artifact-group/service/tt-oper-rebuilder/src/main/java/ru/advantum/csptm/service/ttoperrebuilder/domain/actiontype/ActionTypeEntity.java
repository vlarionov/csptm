package ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ActionTypeEntity extends AbstractEntity<Short> implements ActionType {
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ActionType.class)
    @Fetch(FetchMode.JOIN)
    ActionType parentType;

    String actionTypeCode;

    String actionTypeName;

    @Override
    public boolean instanceOf(short otherId) {
        return getId() == otherId || getParentType() != null && getParentType().instanceOf(otherId);
    }

    @Override
    public boolean isFlexible() {
        return instanceOf(ADJUSTING_TIME)
                || instanceOf(CORRECTION_TIME)
                || instanceOf(SETTLING)
                || instanceOf(DINNER);
//                || instanceOf(BASE_TIME)
//                || instanceOf(MIN_TIME);
    }

}
