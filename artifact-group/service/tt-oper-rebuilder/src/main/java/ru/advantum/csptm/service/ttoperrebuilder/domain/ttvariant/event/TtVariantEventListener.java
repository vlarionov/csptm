package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import java.time.Duration;

public interface TtVariantEventListener {
    void onEvent(TtActionItemDurationChangedEvent event);

    class TtActionItemDurationChangedEvent {
        Long ttActionItemId;

        Duration oldDuration;

        Duration newDuration;

    }
}
