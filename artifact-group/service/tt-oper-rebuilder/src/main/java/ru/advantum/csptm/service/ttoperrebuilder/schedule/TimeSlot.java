package ru.advantum.csptm.service.ttoperrebuilder.schedule;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.time.Duration;
import java.time.Instant;

import static com.google.common.base.Suppliers.memoize;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TimeSlot {
    transient Supplier<Instant> startTimeSupplier;
    transient Supplier<Instant> endTimeSupplier;
    transient Supplier<Duration> durationSupplier;

    public static TimeSlot ofStartTimeDurationProvider(Supplier<Instant> startTimeSupplier, Function<Instant, Duration> durationByStartTimeProvider) {
        return ofStartTimeDuration(startTimeSupplier, () -> durationByStartTimeProvider.apply(startTimeSupplier.get()));
    }

    public static TimeSlot ofStartTimeDuration(Supplier<Instant> startTimeSupplier, Supplier<Duration> durationSupplier) {
        return new TimeSlot(
                startTimeSupplier,
                () -> startTimeSupplier.get().plus(durationSupplier.get()),
                durationSupplier
        );
    }

    public static TimeSlot ofStartTimeEndTime(Supplier<Instant> startTimeSupplier, Supplier<Instant> endTimeSupplier) {
        return new TimeSlot(
                startTimeSupplier,
                endTimeSupplier,
                () -> Duration.between(startTimeSupplier.get(), endTimeSupplier.get())
        );
    }

    private TimeSlot(Supplier<Instant> startTimeSupplier, Supplier<Instant> endTimeSupplier, Supplier<Duration> durationSupplier) {
        this.startTimeSupplier = memoize(startTimeSupplier);
        this.endTimeSupplier = memoize(endTimeSupplier);
        this.durationSupplier = memoize(durationSupplier);
    }


    public Instant getStartTime() {
        return startTimeSupplier.get();
    }

    public Instant getEndTime() {
        return startTimeSupplier.get();
    }

    public Duration getDuration() {
        return durationSupplier.get();
    }
}
