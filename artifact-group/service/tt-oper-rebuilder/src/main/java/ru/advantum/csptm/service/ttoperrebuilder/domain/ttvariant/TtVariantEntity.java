package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(schema = "ttb")
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class TtVariantEntity extends AbstractEntity<Integer> implements TtVariant {

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "ttVariantId",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            targetEntity = TtOutEntity.class
    )
    @Fetch(FetchMode.JOIN)
    @Where(clause = "not sign_deleted")
    Set<TtOut> ttOuts;

    Integer normId;

    @Override
    public void onEvent(TtActionItemDurationChangedEvent event) {
        getTtOuts().forEach(ttOut -> ttOut.onEvent(event));
    }


//
//    public TtVariantEntity(Integer id, Set<TtOut> ttOuts, Integer normId) {
//        super(id);
//        setNormId(normId);
//    }

    //    }
//        return ImmutableSortedSet.copyOf(getTtActions());
//    public SortedSet<TtAction> getSortedTtActions() {
//    @Override

    //    public TtVariantEntity(TtVariant ttVariant) {
//        this(
//                ttVariant.getId(),
//                null,
//                ttVariant.getNormId()
//        );
//        val ttOuts = ttVariant.getTtOuts().stream()
//                .map(TtOutEntity::new)
//                .map(TtOut.class::cast)
//                .collect(Collectors.toSet());
//        getTtOuts().forEach(ttOutEntity -> ttOutEntity.setTtVariant(this));
//    }

//
//
//    @Override
//    public Instant getStartTime() {
//        return null;
//    }
//
//    @Override
//    public Instant getEndTime() {
//        return null;
//    }

//
//
//    @Transient
//    @Value.Lazy
//    public Map<Long, TtActionItem> getTtActionItemIdMap() {
//        return ImmutableMap.copyOf(getContext().getTtActionItems());
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Set<TtActionItem> getTtActionItems() {
//        return ImmutableSet.copyOf(getContext().getTtActionItems().values());
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public NavigableSet<TtActionItem> getSortedTtActionItems() {
//        return ImmutableSortedSet.copyOf(getTtActionItems());
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Map<Integer, Set<TtActionItem>> getTtActionTtActionItemsMap() {
//        Map<Integer, List<TtActionItem>> result = new HashMap<>();
//        for (TtActionItem ttActionItem : getContext().getTtActionItems().values()) {
//            val ttActionItems = result.computeIfAbsent(ttActionItem.getTtAction().getId(), (id) -> new ArrayList<>());
//            ttActionItems.add(ttActionItem);
//        }
//        return result.entrySet().stream().collect(toImmutableMap(
//                Map.Entry::getKey,
//                entry -> ImmutableSet.copyOf(entry.getValue())
//        ));
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Set<TtAction> getTtActions() {
//        return ImmutableSet.copyOf(getContext().getTtActions().values());
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Set<StopItem> getStopItems() {
//        return ImmutableSet.copyOf(getContext().getStopItems().values());
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Set<DrShift> getDrShifts() {
//        return ImmutableSet.copyOf(getContext().getDrShifts().values());
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Instant getStartTime() {
//        return getSortedTtActionItems().first().getStartTime();
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Instant getEndTime() {
//        return getSortedTtActionItems().last().getEndTime();
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public TtAction getLastRound() {
//        val reversedTrActions = trActionStream().sorted(Comparator.reverseOrder());
//        return trRoundStream(reversedTrActions).findFirst().orElse(null);
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public Duration getMaxInterval() {
//        return getRoundWithMaxInterval().getInterval();
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public SortedSet<TtAction> getSortedRounds() {
//        return trRoundStream()
//                .collect(toImmutableSortedSet(Comparator.naturalOrder()));
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public TtAction getRoundWithMaxInterval() {
//        return findRoundWithMaxInterval(Collections.emptySet());
//    }
//
//    @Override
//    @Value.Lazy
//    @Transient
//    public SortedSet<TtAction> getTtActionsOfLastRound() {
//        return ImmutableSortedSet.copyOf(getLastRound().getPreviousTtActions());
//    }
//
//    @Override
//    public TtAction findRoundWithMaxInterval(Set<TtAction> excludedRounds) {
//        return getSortedRounds().stream()
//                .filter(round -> !excludedRounds.contains(round))
//                .max(Comparator.comparing(TtAction::getInterval))
//                .orElse(null);
//    }
//
//    private Stream<TtAction> trActionStream() {
//        return getTtActions().stream();
//    }
//
//    private Stream<TtAction> trRoundStream() {
//        return trRoundStream(trActionStream());
//    }
//
//    private Stream<TtAction> trRoundStream(Stream<TtAction> trActionStream) {
//        return trActionStream
//                .filter(TtAction::isRound);
//    }

}
