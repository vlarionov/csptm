package ru.advantum.csptm.service.ttoperrebuilder.domain;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class AbstractEntity<PK> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    PK id;

}
