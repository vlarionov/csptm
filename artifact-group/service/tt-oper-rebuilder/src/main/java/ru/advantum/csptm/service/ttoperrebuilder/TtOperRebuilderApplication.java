package ru.advantum.csptm.service.ttoperrebuilder;

import com.google.common.cache.CacheBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
//@EnableBinding(IncidentsSaver.class)
public class TtOperRebuilderApplication {

    public static void main(String[] args) {
        SpringApplication.run(TtOperRebuilderApplication.class, args);
    }


}
