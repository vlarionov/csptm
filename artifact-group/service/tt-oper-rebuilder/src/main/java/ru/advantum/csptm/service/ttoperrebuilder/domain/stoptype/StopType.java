package ru.advantum.csptm.service.ttoperrebuilder.domain.stoptype;

import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;

public interface StopType extends Entity<Short> {
    short TERMINAL = 7;
    short TERMINAL_A = 10;
    short TERMINAL_B = 11;


    String getStopTypeName();

}
