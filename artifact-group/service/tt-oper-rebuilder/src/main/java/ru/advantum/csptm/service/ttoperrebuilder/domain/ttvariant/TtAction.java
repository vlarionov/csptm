package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;


import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift.DrShift;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem2round.StopItem2Round;

import java.time.Instant;
import java.util.Set;

public interface TtAction extends Entity<Integer>, TimePeriod, TtVariantEventListener {

    Set<TtActionItem> getTtActionItems();

    TtOut getTtOut();

    ActionType getActionType();

    TtActionItem addTtActionItem(
            Instant timeBegin,
            Instant timeEnd,
            StopItem2Round stopItem2Round,
            DrShift drShift
    );
//
//
//    TtAction getNextTtRoundOnStop();
//
//    Duration getIntervalDiff();
//
////    @Value.Lazy
////    @Transient
//    double getIntervalDiffMultiplier();
//
//    boolean isRound();
//
//    boolean isBreak();
//
//    NavigableSet<TtActionItem> getSortedTtActionItems(); //todo move to out?
//
//    SortedSet<TtAction> getPreviousTtActions();
//
//    TtAction getPreviousTtAction();
//
//    TtActionItem getFirstTtActionItem();
//
//    TtActionItem getLastTtActionItem();
//
//    StopItem getStartStopItem();
//
//    StopItem getEndStopItem();
//
//    TtAction getPreviousTtBreak();
//
//    TtAction getNextRound();
//
//    TtAction getPreviousTtRoundOnStop();
//
//    Duration getInterval();

    Integer getTtOutId();
}
