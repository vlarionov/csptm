package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.stoptype.StopTypeEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtVariant;

import javax.persistence.*;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;

@Entity
@Table(schema = "rts")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StopItemEntity extends AbstractEntity<Integer> implements StopItem {
    @ManyToMany(fetch = FetchType.EAGER, targetEntity = StopTypeEntity.class)
    @Fetch(FetchMode.JOIN)
    @JoinTable(
            schema = "rts",
            name = "stop_item_type",
            joinColumns = @JoinColumn(name = "stop_item_id"),
            inverseJoinColumns = @JoinColumn(name = "stop_type_id")
    )
    @Where(clause = "not sign_deleted")
    Set<StopTypeEntity> stopTypes;

    @Override
    public Set<TtAction> getTtActions() {
        return null;
    }

    @Override
    public TtVariant ttVariant() {
        return null;
    }
}
