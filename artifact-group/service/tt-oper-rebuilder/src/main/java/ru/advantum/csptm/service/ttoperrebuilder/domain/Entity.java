package ru.advantum.csptm.service.ttoperrebuilder.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.common.collect.ImmutableMap;

import javax.persistence.Id;
import java.util.stream.Collector;

import static java.util.function.Function.identity;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")//todo try remove
public interface Entity<PK> {

    static <E extends Entity<ID>, ID extends Number> Collector<E, ?, ImmutableMap<ID, E>> toIdMap() {
        return ImmutableMap.toImmutableMap(Entity::getId, identity());
    }

    @Id
    PK getId();

}
