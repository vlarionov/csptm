package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem2round;

import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem.StopItem;

public interface StopItem2Round extends Entity<Long> {

    StopItem getStopItem();
}
