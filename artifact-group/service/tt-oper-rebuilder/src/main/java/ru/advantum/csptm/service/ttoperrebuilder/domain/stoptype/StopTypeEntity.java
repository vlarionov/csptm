package ru.advantum.csptm.service.ttoperrebuilder.domain.stoptype;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "rts")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StopTypeEntity extends AbstractEntity<Short> implements StopType {

    String stopTypeName;

}
