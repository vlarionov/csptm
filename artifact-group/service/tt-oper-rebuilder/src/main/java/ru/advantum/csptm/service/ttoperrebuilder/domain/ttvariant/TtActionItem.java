package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem2round.StopItem2Round;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift.DrShift;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem.StopItem;

import java.time.Duration;
import java.time.Instant;
import java.util.function.Function;

public interface TtActionItem extends Entity<Long>, TimePeriod {

    StopItem2Round getStopItem2Round();

    StopItem getStopItem();

    DrShift getDrShift();

//    TtActionItem getPreviousTtActionItem();
//
    TtAction getTtAction();
//
//    TtActionItem getNextTtActionItem();
//
//    TtOut getTtOut();
//
//    ActionType getActionType();
//
//    boolean isFlexible();
//
    Integer getTtActionId();

    boolean isFlexible();

    TtActionItem getNextItemInOut();

    void delete();

    void changeDuration(Duration duration, Function<Instant, Duration> roundDurationProvider);
}
