package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem2round;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem.StopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem.StopItemEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "rts")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StopItem2RoundEntity extends AbstractEntity<Long> implements StopItem2Round {

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = StopItemEntity.class)
    @Fetch(FetchMode.JOIN)
    StopItem stopItem;

}
