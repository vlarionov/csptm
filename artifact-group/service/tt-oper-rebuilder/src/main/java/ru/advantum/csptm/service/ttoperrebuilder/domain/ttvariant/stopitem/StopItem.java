package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem;


import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtVariant;

import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;

public interface StopItem extends Entity<Integer> {

    Set<TtAction> getTtActions();

//    NavigableSet<TtAction> getSortedTtActions();
//
//    SortedSet<TtAction> getSortedRounds();
//
//    boolean isFloatingBreakDuration();
//
//    boolean isStartOf(TtAction trAction);
//
//    boolean isTerminal();

    TtVariant ttVariant();

}
