package ru.advantum.csptm.service.ttoperrebuilder.repository.jpa;

import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtVariantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TtVariantRepository extends JpaRepository<TtVariantEntity, Integer> {
}
