package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype.ActionTypeEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem2round.StopItem2Round;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem2round.StopItem2RoundEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift.DrShift;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift.DrShiftEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem.StopItem;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Duration;
import java.time.Instant;
import java.util.function.Function;

@Entity
@Table(schema = "ttb")
@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TtActionItemEntity extends AbstractEntity<Long> implements TtActionItem {
    Integer ttActionId;

    @ManyToOne(targetEntity = TtActionEntity.class, fetch = FetchType.EAGER)
    TtAction ttAction;

    @Setter
    Instant timeBegin;

    @Setter
    Instant timeEnd;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ActionTypeEntity.class)
    @Fetch(FetchMode.JOIN)
    ActionType actionType;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = StopItem2RoundEntity.class)
    @Fetch(FetchMode.JOIN)
    StopItem2Round stopItem2Round;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = DrShiftEntity.class)
    @Fetch(FetchMode.JOIN)
    DrShift drShift;

    @Override
    public StopItem getStopItem() {
        return getStopItem2Round().getStopItem();
    }

    @Override
    public boolean isFlexible() {
        return true;
    }

    @Override
    public TtActionItem getNextItemInOut() {
        return null;
    }

    @Override
    public void delete() {
        getTtAction().getTtActionItems().remove(this);
    }

    @Override
    public void changeDuration(Duration duration, Function<Instant, Duration> roundDurationProvider) {
        this.timeEnd = timeBegin.plus(duration);

    }


//    public TtActionItemEntity(Long id, Integer ttActionId, Instant timeBegin, Instant timeEnd, StopItem2Round stopItem2Round, DrShift drShift) {
//        super(id);
//        this.ttActionId = ttActionId;
//        this.timeBegin = timeBegin;
//        this.timeEnd = timeEnd;
//        this.stopItem2Round = stopItem2Round;
//        this.drShift = drShift;
//    }
//
//    public TtActionItemEntity(TtActionItem ttActionItem) {
//        this(
//                ttActionItem.getId(),
//                ttActionItem.getTtActionId(),
//                ttActionItem.getStartTime(),
//                ttActionItem.getEndTime(),
//                ttActionItem.getStopItem2Round(),
//                ttActionItem.getDrShift()
//        );
//    }


}