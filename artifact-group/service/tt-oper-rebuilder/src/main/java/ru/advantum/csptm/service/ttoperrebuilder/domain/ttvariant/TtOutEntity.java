package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;

import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import ru.advantum.csptm.service.ttoperrebuilder.domain.AbstractEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype.ActionType;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(schema = "ttb")
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Slf4j
public class TtOutEntity extends AbstractEntity<Integer> implements TtOut {

    Integer ttVariantId;

    @ManyToOne(targetEntity = TtVariant.class, fetch = FetchType.EAGER)
    TtVariant ttVariant;

    short ttOutNum;

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "ttOutId",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Fetch(FetchMode.JOIN)
    @Where(clause = "not sign_deleted")
    Set<TtActionEntity> ttActions;
//
//    public TtOutEntity(TtOut ttOut) {
//        this(
//                ttOut.getId(),
//                ttOut.getTtVariantId(),
//                ttOut.getTtOutNum(),
//                ttOut.getTtActions().stream().map(TtActionEntity::new).collect(Collectors.toSet())
//        );
//    }
//
//    public TtOutEntity(Integer id, Integer ttVariantId, short ttOutNum, Set<TtActionEntity> ttActions) {
//        super(id);
//        setTtVariantId(ttVariantId);
//        setTtOutNum(ttOutNum);
//        setTtActions(ttActions);
//    }

    @Override
    public TtAction addTtAction(ActionType actionType) {
        val ttAction = new TtActionEntity(
                null,
                getId(),
                actionType,
                Collections.emptySet()
        );
        ttActions.add(ttAction);
        return ttAction;
    }

    @Override
    public void onEvent(TtActionItemDurationChangedEvent event) {
        ttActions.forEach(ttAction -> ttAction.onEvent(event));
    }
}
