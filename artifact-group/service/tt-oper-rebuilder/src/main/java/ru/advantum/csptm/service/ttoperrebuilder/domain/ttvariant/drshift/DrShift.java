package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift;


import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TimePeriod;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.TtOut;

import java.util.Set;

public interface DrShift extends Entity<Short>, TimePeriod {

    short getDrShiftNum();

    TtOut getTtOut();

    Set<TtAction> getTtActions();

//    SortedSet<TtAction> getSortedTtActions();
//
//    SortedSet<TtAction> getSortedBreaks();

}
