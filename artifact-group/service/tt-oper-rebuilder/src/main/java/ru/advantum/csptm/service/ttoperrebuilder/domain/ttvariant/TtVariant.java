package ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant;


import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.drshift.DrShift;
import ru.advantum.csptm.service.ttoperrebuilder.domain.ttvariant.stopitem.StopItem;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;

public interface TtVariant extends Entity<Integer>, TtVariantEventListener {

    Integer getNormId();

    Set<TtOut> getTtOuts();

//    Set<TtAction> getTtActions();
//
//    SortedSet<TtAction> geSortedTtActions();
//
//    Set<TtActionItem> getTtActionItems();
//
//    NavigableSet<TtActionItem> getSortedTtActionItems();
//
//    Set<StopItem> getStopItems();
//
//    Set<DrShift> getDrShifts();
//
//    SortedSet<TtAction> getSortedRounds();
//
//    TtAction getRoundWithMaxInterval();
//
//    Duration getMaxInterval();
//
//    TtAction getLastRound();
//
//    SortedSet<TtAction> getTtActionsOfLastRound();
//
//    TtAction findRoundWithMaxInterval(Set<TtAction> excludedRounds);
//
//    Map<Long, TtActionItem> getTtActionItemIdMap();
//
//    Map<Integer, Set<TtActionItem>> getTtActionTtActionItemsMap();

}
