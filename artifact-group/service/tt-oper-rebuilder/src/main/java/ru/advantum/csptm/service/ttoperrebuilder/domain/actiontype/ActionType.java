package ru.advantum.csptm.service.ttoperrebuilder.domain.actiontype;

import ru.advantum.csptm.service.ttoperrebuilder.domain.Entity;

//import javax.annotation.Nullable;

public interface ActionType extends Entity<Short> {
    short ROUND = 31;

    short MAIN_ROUND = 1;

    short BREAK = 32;

    short MIN_TIME = 27;

    short ADJUSTING_TIME = 28;

    short CORRECTION_TIME = 29;

    short BASE_TIME = 30;

    short SETTLING = 23;

    short DINNER = 22;

    ActionType getParentType();

//    @Nullable
    String getActionTypeCode();

//    @Nullable
    String getActionTypeName();

    boolean instanceOf(short otherId);

    boolean isFlexible();
}
