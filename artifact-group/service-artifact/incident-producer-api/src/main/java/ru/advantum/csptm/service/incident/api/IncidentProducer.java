package ru.advantum.csptm.service.incident.api;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import ru.advantum.csptm.database.api.CoordsHub;

public interface IncidentProducer {
    String INPUT = CoordsHub.OUTPUT;

    String OUTPUT = "incidents";

    @Input(INPUT)
    SubscribableChannel coords();

    @Output(OUTPUT)
    MessageChannel incidents();

}