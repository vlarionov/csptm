package ru.advantum.csptm.service.incident.api;

import lombok.Value;
import ru.advantum.csptm.database.api.UnitPacket;

@Value
public class Incident {

    String reason;

    UnitPacket unitPacket;

}
