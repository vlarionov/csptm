package ru.advantum.csptm.database.api;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CoordsHub {
    String OUTPUT = "coords";

    @Output(OUTPUT)
    MessageChannel coords();

}