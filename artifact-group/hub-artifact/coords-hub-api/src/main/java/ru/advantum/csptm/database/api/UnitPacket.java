package ru.advantum.csptm.database.api;

import lombok.Value;

@Value
public class UnitPacket {
    double lon;

    double lat;

}
