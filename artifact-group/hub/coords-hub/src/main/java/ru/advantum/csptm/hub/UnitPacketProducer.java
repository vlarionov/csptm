package ru.advantum.csptm.hub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.database.api.UnitPacket;
import ru.advantum.csptm.database.api.CoordsHub;

import java.util.Random;

@Component
public class UnitPacketProducer {
    private final MessageChannel channel;

    private final Random random = new Random();

    private int counter;

    @Autowired
    public UnitPacketProducer(@Qualifier(CoordsHub.OUTPUT) MessageChannel channel) {
        this.channel = channel;
    }


    @Scheduled(fixedRate = 10L)
    public void send() {
        channel.send(MessageBuilder.withPayload(new UnitPacket(random.nextGaussian(), counter++)).build());
    }

}
