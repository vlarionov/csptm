package ru.advantum.csptm.hub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.database.api.CoordsHub;

@SpringBootApplication
@EnableScheduling
@EnableBinding(CoordsHub.class)
public class CoordsHubApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoordsHubApplication.class, args);
    }


}
