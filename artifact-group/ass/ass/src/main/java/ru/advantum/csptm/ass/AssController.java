package ru.advantum.csptm.ass;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping(method = RequestMethod.GET)
public class AssController {
    private static final String TRAFFIC_SQL = "select unit_packet_id, lon, lat from core.traffic";
    private static final RowMapper<TrafficDto> TRAFFIC_ROW_MAPPER = new BeanPropertyRowMapper<>(TrafficDto.class);

    private static final String INCIDENTS_SQL = "select incident_id, reason, unit_packet_id from core.incident";
    private static final RowMapper<IncidentDto> INCIDENTS_ROW_MAPPER = new BeanPropertyRowMapper<>(IncidentDto.class);


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AssController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @RequestMapping(value = "/traffic")
    public List<TrafficDto> getTraffic() {
        return jdbcTemplate.query(TRAFFIC_SQL, TRAFFIC_ROW_MAPPER);
    }

    @RequestMapping(value = "/incidents")
    public List<IncidentDto> getIncidents() {
        return jdbcTemplate.query(INCIDENTS_SQL, INCIDENTS_ROW_MAPPER);
    }

    @Getter
    @Setter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    private static class TrafficDto {
        Long unitPacketId;

        double lon;

        double lat;
    }

    @Getter
    @Setter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    private static class IncidentDto {
        Long incidentId;

        String reason;

        Long unitPacketId;
    }
}
